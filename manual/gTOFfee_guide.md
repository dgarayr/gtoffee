
## Introduction

gTOFfee is a Python implementation of the Energy Span Model developed by Sebastian Kozuch (Kozuch WIRES Comp. Mol. Sci. 2012), recently reformulated in terms of graphs (Kozuch ACS Cat. 2015, Solel, Tarannam & Kozuch ChemComm 2019) as an extension from linear profiles to arbitrarily complex reaction networks.

The core of gTOFfee is separated into two modules:

-   ReaxGraf (**rg**). Handles reaction network I/O and processing, transforming user input into NetworkX.Graph objects. These graphs are treated to generate the required subgraphs (mechanisms and spanning trees) following the Energy Span Model equations, producing a MechanismContainer object.
-   gTOFfee (**gtof**). From a pre-treated reaction network, expressed as a MechanismContainer, applies the ESM to compute the TOF for every single mechanism arising from the network, with or without concentration effects.

A paper detailing foundations and use cases of gTOFfee is available: D. Garay-Ruiz and C. Bo, *ACS Catalysis*, **2020**, 10, 21, 12627–12635, DOI [10.1021/acscatal.0c02332](https://doi.org/10.1021/acscatal.0c02332). 

**Important note**. For every cycle in the network, the final node must have $`G_{reaction}`$ as its energy. Thus, it will correspond to the fully recovered catalyst, only that shifted in energy. 
Also, the *closing edge* which connects the final node with the start point of the cycle must ALSO have $`G_{reaction}`$ as its energy. Is in this way that $`G_{reaction}`$ can be determined for every mechanism, adjusted to different reactant/product concentrations, and allow for the treatment of different products in a single network.

## Dependencies
- Numpy (>= 1.17.3)
- NetworkX (>= 2.4)
- Matplotlib (>= 3.1.2)

## The workflow: quickstart

```python
import nx_reaxgraf as rg
import nx_gTOFfee as gtof

G = rg.graph_instantiator("c4_nodes.txt","c4_edges.txt", closing_edges=["4_1"])
mechanisms = rg.mechanism_fetcher(G)
TOF_list,dE_list,*_ = gtof.TOF_wrapper(mechanisms, temp=298.15,verbose=True)
for item in dE_list:
    print("dE = %6.2f kcal/mol" % item)
```

The basic gTOFfee workflow is summarized in the previous image. The steps to take are:

1.  Import the `nx_reaxgraf` and `nx_gTOFfee` modules.
2.  Instantiate a graph from the network information, stored as plain text.
3.  Obtain the mechanisms derived from the graph.
4.  Compute TOFs and effective energy spans for every mechanism.
5.  Output or process the resulting data.

If concentration effects are to be requested, a dictionary mapping reactant/product labels (appearing on the edge input file) and concentration values must be provided too.
```python
import nx_reaxgraf as rg
import nx_gTOFfee as gtof

G = rg.graph_instantiator("c4_nodes.txt","c4_edges.txt", closing_edges=["4_1"])
mechanisms = rg.mechanism_fetcher(G)
concentrations = {"A":0.5,"B":0.5,"C":0.1}
TOF_list,dE_list,*_ = gtof.TOF_wrapper(mechanisms, temp=298.15,use_conc=True,dict_conc=concentrations,verbose=True)
for item in dE_list:
    print("dE = %6.2f kcal/mol" % item)
```

Some relevant considerations:

1.  Input format. Example files are provided in the `examples/` folder. Two files are required, one for the nodes (intermediates) and one for the edges (transition states.)
    1.  Node input: `Label | Free energy | Connections`. <br>
    The free energies must be in kcal·mol<sup>-1</sup>. <br>
    Connections are given as a comma-separated list of node names. <br>
    The last node of a given cycle *must* correspond to the recovered initial catalyst, displaced $`\Delta G_{reac}`$.
    2.  Edge input: `Edge string | Free energy | Reactants | Products`. <br>
    The edge string is a representation `N1_N2` (with the underscore) specifying one possible sense of traversal of the edge. <br>
    Free energies must be in kcal·mol<sup>-1</sup>. <br>
    Reactants and products are strings specifying which reactant enters or which product leaves in a given edge (if none, specify 'x'). Using edgestrings allows to determine one direction, consistent with the reactant/product definition, and to follow it in further processing. <br>
    The last edge of a given cycle *must* have an energy $`\Delta G_{reac}`$, and be connecting the last node (recovered catalyst) with the initial one (original catalyst). 
        - These requisites allow gTOFfee to process reaction energies ($`\Delta G_{reac}`$) properly, and to adjust them when concentration corrections are requested. 
2.  Additional information. Apart from the input files, there are key parameters about the network that must be given to build the graph.
    -   `closing_edges`. A **list** with all the edges that may close a valid catalytic cycle in our network. For simple cases, there will be only one closing edge, but for intertwined mechanisms with several products a list is required. Having this parameter allows to select only chemically meaningful subgraphs that do correspond to forward, exergonic reactions.
    A valid closing edge should connect the node representing the *recovered catalyst* to the node representing the *initial catalyst* in which the catalytic cycle is assumed to start.
    -   `EUnits`. A **string** defining the units in which the energy values are given. Currently, kcal/mol, kJ/mol and eV are supported, with the keywords `kcal`, `kJ` and `eV`. By default, gTOFfee assumes kcal/mol as the default unit: to change this, user shall modify the `.EUnits` method of the `MechanismContainer` object obtained after mechanism search.
    ```python
    mechanisms = rg.mechanism_fetcher(G)
    mechanisms.EUnits = 'eV' 
    ```

Check the `examples/` folder for more details on input construction. The files `co_nodes.txt` and `co_edges.txt` provide a real use case for the code (Co-catalyzed propene hydroformylation/hydrogenation).
- The nodes **5X** and **9X** correspond to the aforementioned *recovered catalyst* situation.
- Product formation is mapped to the edges that *lead* onto **5X** and **9X**, not to the connections **9X_2** or **5X_2**: the end nodes correspond to the same structure as the start node.

## Details & advanced features

### 1.  System preparation & wrappers. 
The information flow in gTOFfee is: node/edge input (plain text) → `NetworkX.Graph` → *initialized* `MechanismContainer` → *processed* `MechanismContainer`.

The `MechanismContainer` class contains the main network, lists of all the found mechanisms (as `NetworkX.Graph` and `NetworkX.DiGraph` objects) and an analogous list for spanning trees. When it is only *initialized*, only the list of undirected mechanisms is filled, while when it has been already *processed* all the required properties are available (and so the obect is ready for TOF calculation).

The main `TOF_wrapper()` function does a lot of work under the hood. From the *initialized* container:

-   Consider directionality (proper branching patterns and exergonic cycle traversal): generate a "timeline" of every mechanism to know which nodes go before and which nodes go after.
-   Calculation of semi-standard energies to include concentration effects (if requested).
-   Generation of spanning trees.
-   TOF calculation.

If the TOF is going to be computed many times in the same network (e.g. for several concentration values, temperatures...), it will not be necessary to do all the preparation (directionality check, iterative tree search) at every iteration. Instead, a `TOF_wrapper_preinit()` function is provided, which takes pre-existing mechanism and tree structures and recomputes the semi-standard Gibbs free energies accordingly.

### 2. Network plotting. 
As the main network and its subgraphs (mechanisms and spanning trees) are all `NetworkX.Graph` objects, it is possible to employ NetworkX plotting capabilities to easily plot the corresponding graphs. In principle we will be considering the `Matplotlib` interface for simple plots: for very complex networks that cannot be properly visualized with this engine, we reccomend to check the NetworkX documentation for more advanced plotting. <br>
The `rg.plot_directed_network()` function is a quite complete wrapper for the easy plotting network, mechanism or plot. Among the available options (check the corresponding docstring for details), we have:

- Including node/edge energies (either input values or precalculated semi-standard corrected ones).
- Removing node names to obtain graph skeleton plots.
- Direct interface to save the image to a file or just plot it interactively.
- Controlling node positioning. gTOFfee uses the `spring_layout` algorithm from NetworkX to dispose the nodes to be plotted. A keyword can be passed to plot any subgraph with the layout of the parent network. Either, the desired layout can be stored as the `UndirPos` property of the graph to be plotted.<br> 
There is a random state built in layout generation: a seed or a Numpy random state object can be passed to the `nx.spring_layout()` function when the layout is generated to control this issue (check NetworkX documentation).
- Nodes are colored according to its energy: a custom `Matplotlib` colormap can be passed.
- Control transparency of nodes. 


### 3. Binary I/O for pre-built networks. 
A pre-processed network (set of mechanisms and trees arising from a reaction network) can be stored as a Pickle binary object. This allows for an adequate I/O protocol for large networks for which mechanism/tree searches can be quite slow. With the Pickle objects, the full MechanismContainer can be generated in one script that only needs to be run once. Then, that network can be reused for several calculations, plots... saving a lot of time. The `binary_savefile()` and `binary_loadfile()` functions provide a simple interface to generate and load these Pickle objects.

### 4.  Generation of 2D selectivity maps. 
One of the most powerful applications of gTOFfee is to check the effect that changes in the concentrations of reactants and products have on the turnover frequency of the catalytic system.

The application of this feature depends greatly on each user's needs: we provide a set of functions to facilitate the construction of 2D selectivity maps where two variables (concentrations) are scanned at once, for a case where two products are possible.
1.  Input: list of lists of the form `[Label, c_0, c_f, N_elems]`, reference dictionary including also the fixed concentrations of the rest of species, and list of lists containing the indices of the mechanisms leading to either **product 1** or **product 2**.
2.  Calculation: from the list of concentrations, every possible `(c_i,c_j)` pair is generated, and the TOF is recomputed. The resulting turnover frequencies are grouped depending on the product that every mechanism forms. A list of two arrays, with the TOF for **product 1** or **product 2** for each (c<sub>i</sub>,c<sub>j</sub>) pair is obtained. `gtof.TOF_conc_test()`
3.  Representation: from the array of results and the list of labels and concentrations, 2D maps with the quotient TOF(PR1)/TOF(PR2) are constructed. `gtof.plot_matrix_quotient()`

In general, this is a quite specific application of gTOFfee, appliable to a specific type of reaction network (with two and only two products). However, we provide it as part of the main package as a reference for similar concentration-scanning applications that may arise for users. In the future, we would like to generalize this section to more general concentration scans.

### 5. Locating mechanistic typologies.
 In complex networks where many mechanisms can be defined, it is useful to group the mechanisms according to the *main catalytic cycle*, thus defining a set of *typologies* to improve the overall reactivity analysis. 
These typologies can be obtained from an instantiated `MechContainer` object through the `rg.mech_judge()` function, which analyzes the `MainCatCycle` property of every mechanism to obtain all the possible unique cat. cycles, regardless of the branching patterns that vary among individual mechanisms.
This function returns two lists for the mechanism typologies: one containing simplified ('dummy') graph objects and other with the edges defining the main catalytic cycles, in tuple form.
Once `rg.mech_judge()` has been applied, the resulting mechanism typologies can be used for:
- Assignment of typology indices as properties for the individual mechanisms, through `rg.mech_typology()`.
- Combination of TOF values per typology to have insights on the feasibility of the combined reaction route.
- Plotting of all found catalytic cycles, via `rg.mech_typology_plot()`: for every typology, the involved edges are colored over a simplified depiction of the network.

### 6. Interactive plotting
The InteractProfiles (**intp**) module, built on top of the Panel library, permits the easy generation of interactive, reactive visualizations of the free energy profiles for each mechanism in the network.
Currently, two reactive parameters are supported: transition state energies and concentrations. The user should specify whose edges (TSs) and whose concentrations are to be modified: the InteractProfiles library then generates the appropriate visualization integrating sliders for these properties.

Due to the idiosyncrasy of Panel, these interactive applications must be built on a Jupyter Notebook: however, the panel might be generated either in-line or deployed as a separate window.

An example notebook is provided in `examples/InteractiveCoProfile.ipynb`, detailing the few lines of code which are necessary to generate the interactive plots.

![](panel_example.png)

## Some more about implementation

The previous sections focus mostly on how to run gTOFfee. Here, we intend to describe a bit better how the program is structured and what individual tasks are required to accomplish TOF calculation. This is aimed to describe the computational implementation: for more details on the underlying model, we refer to the accompanying paper: Garay-Ruiz & Bo, 2020, ACS Catal. (just accepted).

1.  `rg.graph_instantiator()`
    -   Data reading. Node file is parsed, assigning energies and connections. Then edge file is parsed, extracting all edge energies, reactants and products. The program *checks* whether all connections in the node file match the defined edges, and raises a flag if they do not.
    -   Graph generation. Generates the `NetworkX.Graph` object from the read data, assigning properties to nodes and edges. The list of closing edges is saved as a graph property.
2.  `rg.mechanism_fetcher()`
    -   Initializes the main `MechanismContainer` object that will be carried along the whole run, and assigns the main graph to it.
    -   Through the minimal basis of cycles assigned to the main graph, the number of edges that must be removed to generate mechanisms is determined. Then, all possible edge combinations are generated.
    -   Iterative mechanism testing. For every edge combination, the subgraph missing those edges is generated and tested for the three main mechanism conditions.
        -   All nodes must be connected.
        -   There must be one and only one cycle.
        -   The mechanism must not be equivalent to any previous one.
    -   Regarding the equivalence check, what is done is to check for *isomorphism* including the **node label** as a property, between the currently tested mechanism and the whole list of accepted ones.
    -   For every mechanism, a set of important properties is now saved as graph attributes:
        -   **RemovedEdge**. Edges taken out from the main graph.
        -   **MainGraphLayout**. For plotting, carry out the same layout as the main network.
        -   **StartNode**. Where to start cycle traversals: assigned to the end node of the closing edge.
        -   **ClosingEdge**. Which of the possible closing edges does close this specific mechanism, decided upon mechanism generation and cycle detection.
        -   **GRecipe**. Formula to obtain the reaction free energy, depending on the specific cycle and closing edge.
        -   **MainCatCycle**. List of nodes participating in the cycle.
        -   **Sequences**. Lists of nodes used to direct the mechanism.
        -   **EUnits**. Units of the energy values. By default, uses `kcal` (kcal/mol), can also be `kJ` (kJ/mol) or `eV`.
    -   Found mechanisms are finally appended to the `.MechList` attribute of the container.
3.  `gtof.TOF_wrapper()`
    -   As aforementioned, this function does a lot of critical preparation tasks under the hood, which we shall go through now.
    -   `rg.graph_director()`
        -   Generation of an acyclic subgraph through closing edge removal and location of all end nodes in there.
        -   Look for the unique sequences connecting each end node with the mechanism's start node. Also handle where branches depart from the main cycle.
        -   Use all sequences to generate a *directed* analogue of the mechanism:
            -   For cyclic nodes, *exergonic* direction.
            -   For branches, from the in-cycle branching point to branch end.
        -   Assign reactants and products to every mechanism, comparing the input direction of the edge (saved as the **edgestring** property) with the true traversal direction decided here: assigns **Rx** and **Px** attributes to all edges.
    -   Append the directed mechanism to the container, as part of the **DirMechList** attribute.
    -   `rg.timeline()`
        -   According to the traversal direction embedded in the directed graph, assign proper lists of nodes, edges, reactants and products that appear *before* or *after* every node and edge.
        -   As in the previous function, get the acyclic sub-graph removing the closing edge from the mechanism.
        -   For every node, do a *depth-first-traversal* in the subgraph, getting a nested dictionary for node successions. From this, via iteration we can get plain lists of the nodes and edges **after** any given *node*.
        -   Still in the *node loop*, for the structures *before*, we should come back to the sequences used for directionality assignment. For all off-cycle nodes we will use the in-cycle branching point as reference. Analyzing the sequences up to the reference point (in-cycle node or branching point) it is easy to get the nodes and edges **before** our given node.
        -   This information is stored as **EdgesBefore** and **EdgesAfter** attributes, per *node*. Then we can easily analyze the **Rx** and **Px** properties of these edges, building the attribute lists **ReacsAfter** and **ProdsBefore**.
        -   Our mapping of ***X-Before*** and ***X-After*** properties only extends to *nodes*. To consider also the "timeline" of edges, we just need to go along them, using its first node as the *before* reference and the second as the *after* reference.
        -   Finally, the timelined mechanism can be converted back to a simpler, undirected graph containing all new attributes.
        -   Summary: timeline-related properties for nodes and edges mapped throughout this function.
            -   **EdgesBefore**
            -   **EdgesAfter**
            -   **ReacsAfter**
            -   **ProdsBefore**
            -   All of these are simple, plain lists. Other analogous properties, such as NodesBefore, NodesAfter, ReacsBefore or ProdsAfter could be easily derived, but are not required by our implementation.
    -   Update the `.MechList` attribute with a new list with the traversed, timelined undirected graphs we have just obtained.
    -   Energy correction. All energy-calculating functions require an **EnergyCorr** property so as to handle concentration effects. For the concentrationless case, we have a detour here.
        -   *No concentrations*. `set_default_energy()` will just assign **EnergyCorr** for every node & edge to the **energy** property.
        -   *With concentrations*. `semi_standard_calculator()` will process a mechanism to include the correction at a given temperature and with the reactant/product concentrations that we specify in a dictionary. The expression is: 
        
        ```math
        \Delta G^{\emptyset} (I_j) = I_j^{\emptyset} = I_j + RT \log \left( \prod_{h=j}^{N} [R]_h \prod_{h=1}^{j-1} [P]_h \right)
        ```
        ```math
        \Delta G^{\emptyset} (T_i) = T_i^{\emptyset} = T_i + RT \log \left( \prod_{h=i+1}^{N} [R]_h \prod_{h=1}^{i-1} [P]_h \right)
        ```
        The indices in the multiplicative terms imply that only the reactants consumed after the node/edge and the products formed before the node/edge affect the semi-standard energy. Through the **ReacsAfter** and **ProdsBefore** lists and the dictionary, the products in the equations above can be easily computed, assigning the results to the **EnergyCorr** attribute.

    -   `Greac_assignment()`. By now, only a formula to compute reaction free energy depending on the closing edges has been defined every mechanism (**GRecipe** attribute). Now, we can use the semi-standard energies (if requested) to assign a value for **Greaction** for every mechanism, using the **EnergyCorr** properties for the involved edges.
    -   Spanning tree generation. This comes in two steps:
        -   `span_tree_generator()`. From a list of mechanisms, remove in-cycle edges iteratively to generate acyclic subgraphs. These tree candidates are checked just like mechanisms were:
            -   All nodes are connected.
            -   The graph is acyclic.
            -   The graph is not equivalent to an already accepted tree.
        -   `span_tree_closer()`. From the full list of spanning trees, for each one find the first edge that will close the tree to one of the valid mechanisms. In this sense, first we must find the mechanisms that only differ from the tree in **one** edge. Then, the "first-edge" criterion depends on the number of **EdgesAfter** of every candidate: the one with more **EdgesAfter** will be selected. This step is essential to assign *δ* afterwards.
    -   `TOF_multimech()`. This is the "main" function of the whole code, applying the equation by S. Kozuch to get the turnover frequency for the reaction network.
    
    ```math
    TOF = \frac{k_B T}{h} \sum_{n} \frac{\mu_{n} (1 - e^{\Delta G_r /RT})}{(\sum_k \tau_k) (\sum_j e^{(-I_j+\delta_{ij})/RT})}
    ```

    -   Both *τ*<sub>k</sub> and *μ*<sub>n</sub> are simple exponential terms exp (∑ T<sub>i</sub>/RT) extended to all edges in the corresponding subgraph (mechanism or tree)
    -   For the denominator, every tree is processed, computing the corresponding exponentials and assigning *δ* for the intermediates according to the matched mechanisms. N<sub>tree</sub> sized arrays are obtained for the *τ*<sub>k</sub> and exp (-I+*δ*) terms.
    -   Then, the exponential term *μ*<sub>n</sub> corresponding to the mechanism and the driving force (numerator) are also computed.
    -   Finally, array denom. terms are added and the final quotient is computed, obtaining the TOF in s<sup>-1</sup>. The effective energy span *δ*E<sub>eff</sub> is determined too.

        
## Troubleshooting

In most cases, graph generation and mechanism search can proceed without issues even for networks that do not comply with the required input format. However, the TOF calculation will not work for these cases, and thus the program will abort after calling `gtof.TOF_wrapper()`. Some common mistakes in network definition that will result on non-computable TOFs are:
- Bad definition of closing edges. When `rg.graph_instantiator()` is called, a list of edgestrings defining *all* possible closing edges must be provided. If edges that do close a catalytic cycle are missing from this list, the program will fail when trying to assign the directionality of these mechanisms (upon calling `rg.graph_director()`). If this happens, be sure to check that all possible closing edges are properly passed when the graph is generated.
- Uncompliant last node. In this implementation, it is essential that the last node of every mechanism corresponds to the *recovered catalyst*.
    - Its energy must be $`G_{reaction}`$ for the corresponding reactive route.
    - The node must have only two connections: to the corresponding start node (initial catalyst, through the *closing edge*) and to a single on-cycle node.

    If the second condition (two connections only) is not fulfilled, the program will fail when determining the 'timeline' of the mechanism (upon `rg.timeline()` call). An error message will be raised explaining the issue. 

    This might happen if an on-cycle intermediate is assumed to be directly connected to the start node, without having defined the required recovered catalyst state.



