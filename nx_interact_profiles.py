### Dependencies

import nx_reaxgraf as rg
import nx_gTOFfee as gtof
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import panel as pn
from functools import partial

### Required functions

def subgraph_energy_editor(mech_container,edge_labels,edge_energies):
    '''Modify requested energy values across all mechanisms and trees from a
    MechanismContainer object, leaving the MainGraph untouched for reference.
    Input:
    - mech_container. Traversed MechContainer object with defined trees
    - edge_labels. List of tuples, defining the edges whose energies are being modified
    - edge_energies. List of floats, with th new energies for the requested edges
    Output:
    - None. Modifies the mech_container in-place'''
    for edge,e_edge in zip(edge_labels,edge_energies):
        for mx in mech_container.MechList:
            if (mx.has_edge(*edge)): 
                mx[edge[0]][edge[1]]['energy'] = e_edge
        for st in mech_container.SpanTrees:
            if (st.has_edge(*edge)): 
                st[edge[0]][edge[1]]['energy'] = e_edge
    return None

def format_results_basic(results):
    '''
    Basic formatter for lists of results, prepared for pretty-printing of cases where
    a P1/P2/Ratio 3-element list is generated (else, falls back to a more general case)
    Input:
    - results. List of floats containing some computed values.
    Output:
    - out_string. Simple formatted string.
    '''
    if (len(results) == 3):
        #assume a Prod1/Prod2/Ratio scheme
        res_strings = tuple(["%6.2f" % entry for entry in results])
        #use a multiline string for markdown
        out_string = "P1 = %s ; P2 = %s ; ratio = %s" % res_strings
    else:
        res_strings = tuple(["%6.2f" % entry for entry in results])
        out_string = ";  ".join(res_strings)
    return out_string

def interactive_TOF_calculator(mech_container,edge_labels,edge_energies,temp,ndx,use_cx=False,
                          cREF=None,conc_labels=None,conc_values=None,calc_function=None,
                          ylimit=(-30,20),*args):
    '''
    Interface to run TOF calculations linked to widgets to modify requested transition state energies
    and reactant/product concentrations.
    Input:
    - mech_container. Pre-computed MechanismContainer.
    - edge_labels. List of tuples defining the edges whose energies will be modified.
    - edge_energies. List of floats, numeric values for the edges. LINKED to widget.
    - temp. Float, temperature in K.
    - use_cx. Boolean. If True, consider changes in concentrations.
    - cREF. Dictionary of string:float pairs to use as reference for concentrations.
    - conc_labels. List of strings defining the concentrations that will be modified.
    - conc_values. List of floats, numeric values for concentrations. LINKED to widget.
    - calc_function. Function that will be used to join the TOFs. Accepts *args.
    - ylimit. Tuple of floats, defining limits for the y-axis.
    Output:
    - fig. Matplotlib figure intended for interactive plotting with Panel, including annotations
    for the joined eff. energy spans and to identify the best mechanism.
    '''

    # modify energies
    subgraph_energy_editor(mech_container,edge_labels,edge_energies)

    # concentrations
    if (use_cx == True):
        cx_dict = cREF.copy()
        for sp,c_sp in zip(conc_labels,conc_values):
            cx_dict[sp] = c_sp
        A,B,*_ = gtof.TOF_wrapper_preinit(mech_container,temp,True,cx_dict,verbose=False)
    else:
        A,B,*_ = gtof.TOF_wrapper_preinit(mech_container,temp,verbose=False)
        
    m_plottable = mech_container.MechList[ndx]
    
    # allow to compute a joined TOF
    if (calc_function != None):
        results = calc_function(A,*args)
    else:
        results = []
    
    # plotting
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    
    # profile for requested mechanism, with current eff. E span as title
    rg.unfold_profiles(m_plottable,fig,ax,e_string='EnergyCorr')
    e_title = "Mech. %d: $\delta E_{eff}$ = %6.2f kcal/mol" % (ndx,B[ndx])
    ax.set_title(e_title)
    ax.set_ylim(ylimit)
    
    # find the best-performing mechanism
    best_mech_ndx = np.argmin(B)
    best_mech = B[best_mech_ndx]
    best_mech_note = "Best mech. is %d, /w $\delta E_{eff}$ = %6.2f kcal/mol" % (best_mech_ndx,best_mech)
   
    # annotations: process results
    note = format_results_basic(results)
    # if we only have a single dE value, add TOF and units
    if (len(results) == 1):
        tof_note = " (TOF = %6.4f s-1)" % np.sum(A)
        note = note + " %s" % mech_container.EUnits + tof_note

    ax.annotate(note,xy=(0.5,-0.15),xycoords='axes fraction',ha='center',fontsize=12)
    ax.annotate(best_mech_note,xy=(0.5,-0.30),xycoords='axes fraction',ha='center',fontsize=12)
    plt.tight_layout()
    plt.close(fig)

    return fig

def get_network(G):
    '''
    Simple plotting function for a Graph object, wrapping sensible layout generation and simple input
    parameters.
    Input:
    - G. nx.Graph object.
    Output:
    - fig. Matplotlib figure.
    '''
    posx = nx.spring_layout(G,k=0.5,iterations=1000,seed=42)
    G.graph['MainLayout'] = posx
    fig,ax = rg.plot_directed_network(G,figsize=(6,6),no_labels=False,alpha_val=0.6)
    plt.close(fig)
    return fig

def get_network_mech(mech_container,ndx):
    '''
    Plotting function to overlay the current catalytic route over the graph.
    Input:
    - mech_container. The MechContainer object used in calculations
    - ndx. Integer, mech. to be plotted.
    Output:
    - fig. Matplotlib figure.
    '''
    try:
        posx = mech_container.MainGraph.graph['MainLayout']
    except:
        posx = nx.spring_layout(mech_container.MainGraph,k=0.1,iterations=2000,seed=42)
        mech_container.MainGraph.graph['MainLayout'] = posx
        
    fig,ax = rg.plot_directed_network(mech_container.MainGraph,figsize=(6,6),no_labels=True,alpha_val=0.6)
    cycle = mech_container.MechList[ndx].graph['MainCatCycle']
    # the overlay
    rg.nx.draw_networkx_edges(mech_container.MainGraph,posx,ax=ax,
                              edgelist=cycle,width=5.0,edge_color="#ff69b4")
    plt.close()
    return fig

def slider_generator(G,target_edges,slider_range=None):
    '''
    Automatized generation of sliders for TS energies, using the default values in
    the graph as reference.
    Input:
    - G. Graph object to fetch default values from.
    - target_edges. List of tuples defining the edges whose energies will be modified.
    - max_barr. Float, maximum value for the sliders.
    - slider_range. Tuple of floats, with max. value and step for TSs
    Output:
    - slider_list. List of widget.FloatSlider objects to control all requested edge energies.
    '''
    # handle default values for slider range
    if (slider_range == None):
        slider_range = (30.0,1.0)
    #employ the graph to set reasonable defaults
    slider_list = []
    for target in target_edges:
        edge_name = "%s_%s" % target
        default_energy = G[target[0]][target[1]]['energy']
        min_energy = G.nodes[target[0]]['energy']
        slider = pn.widgets.FloatSlider(name=edge_name,start=min_energy,
                                       step=slider_range[1],end=slider_range[0],value=default_energy,width=100,
                                       bar_color="#4169E1")
        slider_list.append(slider)
    return slider_list

def slider_conc_generator(target_concs,cREF,max_conc=5.0):
    '''
    Automatized generation of sliders for concentration values..
    Input:
    - target_concs. List of tuples defining the reacs/prods whose concentrations will be modified.
    - cREF. Dictionary of string:float pairs to use as reference for concentrations.
    - max_conc. Float, maximum value for the sliders.
    Output:
    - slider_list. List of widget.FloatSlider objects to control all requested cncentrations.
    '''
    slider_list = []
    for target in target_concs:
        c_target = cREF[target]
        c_max = max(max_conc,2*c_target)
        slider = pn.widgets.FloatSlider(name=target,start=0.01,end=c_max,value=c_target,width=100,
                                        bar_color="#DDA0DD")
        slider_list.append(slider)
    return slider_list


def naive_sum(tof_list,temp,eunits='kcal'):
    '''
    Basic combination of all TOFs and generation of the
    corresponding eff. E span
    Input:
    - tof_list. List of TOF values for all mechanisms.
    - temp. Float, temperature in Kelvin.
    - eunits. String, defining the units to compute dEeff (kcal/eV/kJ)
    Output:
    - [B_join]. List of floats, containing the joined eff. energy span.
    '''
    A_join = np.sum(tof_list)
    B_join = gtof.eff_E_span_calc(A_join,temp,eunits)
    return [B_join]

def tof_joiner(tof_list,route_indices,temp,eunits='kcal'):
    '''
    From a list of TOFs corresponding to two different products,
    join according to the formed product and return the eff. energy spans
    Input:
    tof_list. List of TOF values for all mechanisms.
    route_indices. List of two lists, each one containing all mech. indices corresponding to one or other product.
    temp. Float, temperature in Kelvin.
    Output:
    dE_1,dE_2. Floats: eff. energy spans for products 1 and 2
    '''
    tofs1=np.array(tof_list)[route_indices[0]]
    tofs2=np.array(tof_list)[route_indices[1]]
    dE_1=gtof.eff_E_span_calc(np.sum(tofs1),temp,eunits)
    dE_2=gtof.eff_E_span_calc(np.sum(tofs2),temp,eunits)
    ratio = np.sum(tofs1)/np.sum(tofs2)
    return dE_1,dE_2,ratio

### Main wrapper

def panel_wrap(mech_container,used_TS,temp,use_cx=False,used_concs=None,cREF=None,
               calc_function=None,deploy=False,TS_slider_range=None):
    '''
    Automated panel generation to have a interactive plot of the reaction energy profile corresponding
    to a valid reaction mechanism, controlling requested transition state energies and species concentrations.
    Input:
    - mech_container. MechContainer object.
    - used_TS. List of tuples defining the edges whose energies will be modified.
    - temp. Temperature in K.
    - use_cx. Boolean, if True also modify concentrations.
    - used_concs. List of tuples defining the specices whose concentrations will be modified.
    - cREF. Dictionary of string:float pairs to use as reference for concentrations.
    - calc_function. Function to join TOFs.
    - deploy. Boolean, if True, generate a separate Bokeh server for the plot.
    Output:
    - out_panel. Panel object containing the interactive plot, the required sliders and textboxes,
    and the formatted values from calc_function.
    '''

    # 1. Build sliders and required widgets
    mbox = pn.widgets.TextInput(name='mechanism',value='0',width=80)
    checkbox = pn.widgets.Checkbox(name="View mechanism overlay")
    # limits for the box
    inf_lim = pn.widgets.TextInput(name='lower',value='-40',width=50)
    sup_lim = pn.widgets.TextInput(name='upper',value='20',width=50)

    # use MainGraph from the MechContainer, which has the original energies, as reference
    slidersTS = slider_generator(mech_container.MainGraph,used_TS,TS_slider_range)
    default_TS_energies = [slxTS.value for slxTS in slidersTS]
    
    # for concentration effects
    if (use_cx == True):
        slidersC = slider_conc_generator(used_concs,cREF)
        target_tuple = tuple([slxTS.param.value for slxTS in slidersTS] + [slxC.param.value for slxC in slidersC])
        init_values = [slxTS.value for slxTS in slidersTS] + [slxC.value for slxC in slidersC]
    else:
        target_tuple = tuple([slxTS.param.value for slxTS in slidersTS])
        init_values = [slxTS.value for slxTS in slidersTS]
        
    # 2. Define the reactive function with @pn.depends decorator, returning a single Matplotlib
    # figure for the visualization

    @pn.depends(mbox,inf_lim,sup_lim,*target_tuple)
    def reactive_plot(mbox,inf_lim,sup_lim,*target_tuple,mechs=mech_container,edgelist=used_TS,
                      conclist=used_concs,N_TS=len(slidersTS),calc_function=calc_function):
        #process interactive variables: obtain the integer for mech. index and divide slider value lists
        ndx = int(mbox)
        ylim_tuple = (float(inf_lim),float(sup_lim))
        if (use_cx == True):
            #convert text input to string
            TS_tuple = target_tuple[0:N_TS]
            C_tuple = target_tuple[N_TS:]
            fig = interactive_TOF_calculator(mech_container=mechs,edge_labels=edgelist,
                                        edge_energies=list(TS_tuple),temp=temp,
                                        ndx=ndx,use_cx=True,cREF=cREF,conc_labels=conclist,
                                        conc_values=list(C_tuple),calc_function=calc_function,
                                        ylimit=ylim_tuple)
        else:
            TS_tuple = target_tuple
            fig = interactive_TOF_calculator(mech_container=mechs,edge_labels=edgelist,
                                            edge_energies=list(TS_tuple),temp=temp,
                                            ndx=ndx,use_cx=False,calc_function=calc_function,
                                            ylimit=ylim_tuple)
        return pn.Column(fig)
    
    # Definition of a simple reset button to recover initial TS energy values
    # directly modifying the sliders
    # Workflow: create widget, define reactive event and add as on-click property
    reset = pn.widgets.Button(name='Reset',button_type='primary',width=100)
    def reset_button(event):
        # make it work also without concentrations
        if (use_cx == True):
            all_sliders = slidersTS + slidersC
        else:
            all_sliders = slidersTS
        for slider,initial in zip(all_sliders,init_values):
            slider.value = initial
        return None
    reset.on_click(reset_button)
    
    # axis limits
    ylim_control = pn.Row(inf_lim,sup_lim)

    # 3. Define the layout
    # Use an interactive function for toggling between network and mech. cycle overlay
    @pn.depends(mbox,checkbox)
    def toggle_mech_overlay(mbox,checkbox):
        ndx = int(mbox)
        if (checkbox):
            # draw the overlay
            fig = get_network_mech(mech_container,ndx)
        else:
            # plot only main network
            fig = get_network(mech_container.MainGraph)
        return fig

    network_widget = pn.Column(checkbox,toggle_mech_overlay)
    slider_col = pn.Column(*slidersTS,mbox,reset,"y-axis limits",ylim_control)
    
    if (use_cx == True):
        C_row = pn.Row(*slidersC)
        main_column = pn.Column(reactive_plot,C_row)
        out_panel = pn.Row(network_widget,main_column,slider_col)
    else:
        main_column = pn.Column(reactive_plot)
        out_panel = pn.Row(network_widget,main_column,slider_col)
    
    # 4. Return object, deploying it as a Bokeh server or not
    if (deploy == True):
        return out_panel.show(port=54322)
    else:
        return out_panel