'''Basic SetupTools-based installation script for gTOFfee'''
from setuptools import setup,find_packages
setup(name='gTOFfee',
      version='4.0',
      author="Diego Garay-Ruiz",
      url='gitlab.com/dgarayr/gtoffee',
      description="TOF calculation for reaction networks, following the energy span model",
      py_modules=['nx_reaxgraf','nx_gTOFfee','nx_interact_profiles'],
      install_requires=['numpy','matplotlib','networkx','panel'])
