import nx_reaxgraf as rg
import nx_gTOFfee as gtof
import numpy as np
import matplotlib.pyplot as plt

###PARAMETERS
temp=423.15
c0={"CO":0.2,"H2":0.1,"ALK":0.5,"PR1":0.01,"PR2":0.01,"x":None}

###Initialize the graph and plot it
G=rg.graph_instantiator("co_nodes.txt","co_edges.txt",
						closing_edges=["9X_2","5X_2"])
rg.plot_directed_network(G,parent_layout=False)
plt.show()

###Mechanism search & TOF calculation
mechanisms=rg.mechanism_fetcher(G)
A,B,C,D=gtof.TOF_wrapper(mechanisms,temp,use_conc=True,
						 dict_conc=c0,verbose=False)
###TOF data processing
#Assignation of alkane-producing and aldehyde-producing routes
hydroform=[]
hydrogen=[]
routes=[hydroform,hydrogen]
for im,m in enumerate(mechanisms.MechList):
	closer=m.graph['ClosingEdge']
	if (closer == ('5X','2')):
		hydrogen.append(im)
	else:
		hydroform.append(im)

#Output
tof_hydroform=np.array(A)[hydroform]
tof_hydrogen=np.array(A)[hydrogen]
dE_hydroform=gtof.eff_E_span_calc(np.sum(tof_hydroform),temp)
dE_hydrogen=gtof.eff_E_span_calc(np.sum(tof_hydrogen),temp)
print("|Aldehyde|%6.2f kcal/mol|" % (dE_hydroform))
print("|Alkane|%6.2f kcal/mol|" % dE_hydrogen)

#Individual mechanism representation
for im,mech in enumerate(mechanisms.DirMechList):
	fig1,ax1=rg.plot_directed_network(mech,parent_layout=True)
plt.show()	
